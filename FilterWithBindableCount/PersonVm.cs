﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgentOctal.WpfLib;

namespace FilterWithBindableCount
{
    class PersonVm:ViewModel
    {
        private string _firstName;
        public string FirstName
        {
            get {return _firstName;}
            set {SetValue(ref _firstName, value);}
        }

        private string _lastName;
        public string LastName
        {
            get {return _lastName;}
            set {SetValue(ref _lastName, value);}
        }
    }
}
