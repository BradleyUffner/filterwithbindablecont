﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using AgentOctal.WpfLib;

namespace FilterWithBindableCount
{
    class MainWindowVm : ViewModel
    {
        public MainWindowVm()
        {
            People = new ObservableCollection<PersonVm>();
            PeopleView = (CollectionView)CollectionViewSource.GetDefaultView(People);
            PeopleView.Filter = obj =>
            {
                var person = (PersonVm)obj;
                return person.FirstName.ToUpper().Contains(FilterText.ToUpper() ) || person.LastName.ToUpper().Contains(FilterText.ToUpper());
            };

            People.Add(new PersonVm() { FirstName = "Bradley", LastName = "Uffner" });
            People.Add(new PersonVm() { FirstName = "Fred", LastName = "Flintstone" });
            People.Add(new PersonVm() { FirstName = "Arnold", LastName = "Rimmer" });
            People.Add(new PersonVm() { FirstName = "Jean-Luc", LastName = "Picard" });
            People.Add(new PersonVm() { FirstName = "Poppa", LastName = "Smurf" });

        }
        public ObservableCollection<PersonVm> People { get; }
        public CollectionView PeopleView { get; }

        private string _filterText = "";
        public string FilterText
        {
            get => _filterText;
            set
            {
                if (SetValue(ref _filterText, value))
                {
                    PeopleView.Refresh();
                }
            }
        }

    }
}
